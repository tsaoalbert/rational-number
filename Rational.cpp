// #include "stdafx.h"
#include <iostream>
#include "Rational.h"
using namespace std;

/*
  Rules of Arithmetic Operators for Rationl Numbers
  Given x = a/b, y = c/d,

  x+y = (a*d+c*b) / ( b*d) 
  x-y = (a*d-c*b) / ( b*d) 
  x*y = (a*c) / ( b*d) 
  x/y = (a*d) / ( b*c) 

  x==y  iff   a*d==c*b
  x<=y  iff   a*d<=c*b
*/

// By using the default parameter settings in Rational.h, this
// constructor also provides the default constructor Rational()
Rational::Rational (int num, int denom)
{ 
    setRational(num,denom);   // set numerator and denominator, reduce fraction, fix the sign 
}

// Helper function to fix a zero denominator and fix the sign if denominator is negative
Rational Rational::setRational (int n, int d) // helper function
{
    numerator = n;
    denominator = d;

    // if denominator == 0 then set it = 1
    if (denominator == 0)
        denominator = 1;

    if ( denominator < 0 ) // if denominator is neg, multiply num and denom by -1
    {
        numerator = -numerator;     // fix sign of numerator +/-
        denominator = -denominator; // denominator always +
    }

    int gcd = GCD(numerator, denominator);
    if (denominator != 0)
    {
        numerator /= gcd;
        denominator /= gcd;
    }
    return *this;   // return the current object
}

// find the lowest common divisor using a recursive function
int Rational::GCD(int v1, int v2)
{
    if (v2==0) return v1;
    else return GCD (v2, v1%v2);
}

Rational Rational::add (Rational y)
{
	int a = numerator;
	int b = denominator;
	int c = y.numerator;
	int d = y.denominator;

    // create a new Rational object and return it
    return Rational(a*d+c*b, b*d);
}

// the operator+ method does the same thing as the add method
Rational Rational::operator+ (Rational y)
{
    return add ( y );
}

Rational Rational::operator+= (Rational right)
{
    // the current object is updated with the result of the += 
    numerator = numerator*right.denominator + right.numerator*denominator;
	denominator = denominator * right.denominator;

    // fix the sign, reduce the fraction and return the current object
    return setRational(numerator, denominator);
}

Rational::operator double() const // convert Rational to double and return
{
    return  0;
}

// Display a Rational number using the display() member method
void Rational::display()
{
	cout << numerator << '/' << denominator;
}

// Display a Rational number using << and a friend function.
// Friend functions are not part of the class and their code must be
// declared outside of the class with no :: Scope Resolution Operator.
// All function arguments must have their class defined 
ostream &operator<< (ostream &out, Rational const &r)
{ 
    out << r.numerator << '/' << r.denominator; 
    return out; // This is to keep the stream flowing
}
