#pragma
// #include "stdafx.h" // use only for Microsoft Visual Studio C++
/*
  Rules of Arithmetic Operators for Rationl Numbers
  Given x = a/b, y = c/d,

  x+y = (a*d+c*b) / ( b*d)
  x-y = (a*d-c*b) / ( b*d)
  x*y = (a*c) / ( b*d)
  x/y = (a*d) / ( b*c)

  x==y  iff   a*d==c*b
  x<=y  iff   a*d<=c*b
*/

#include <iostream>
using namespace std;

class Rational
{
    // Friend functions are actually declared outside the scope of the
    // class but have the right to access public and private data and
    // member function members that belong to the class. The friend
    // function below gives the << operator for ostreams (including cout)
    // the ability to output a Rational object by accessing its member data.
    friend ostream &operator<< (ostream &out, Rational const &r);

public:
	Rational (int num=0, int denom=1);  // also provides default constructor

  Rational add (Rational right);
	Rational operator+  (Rational right);    // + addition operator
	Rational operator+= (Rational right);   // += addition assignment operator
	void display();
    operator double() const; // convert Rational to double

private:
	int numerator;
	int denominator;
    // helper functions are private and not accessible by the main program
    int GCD(int v1, int v2);
    Rational setRational (int n, int d);
};
